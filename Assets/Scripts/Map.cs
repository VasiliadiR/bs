﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map : MonoBehaviour
{
    public static event Action<int,int> Remaining;

    public static List<Vector3> mapBalls = new List<Vector3>();
    public static List<GameObject> roofSphers = new List<GameObject>();
    public static List<GameObject> connected = new List<GameObject>();
    public static int counter;

    public Pool redPool;
    public Pool bluePool;
    public Pool violetPool;
    public Pool turqoisePool;
    public Pool roofPool;
    public TextAsset textMapAsset = null;

    private string textMap;
    private int _width = 14;
    private int _height = 14;
    private int _d;
    private GameObject[] _balls;

    private void Start()
    {
        Ball.Reconfig += Apoptosis;
        _d = Repository.ballDistance;
        _width = Repository.width;
        _height = Repository.height;
        textMap = textMapAsset.text.Replace("\r", "");
        textMap = textMap.Replace("\n", "");
        textMap = textMap.Replace(" ", "");

        for (int k = _height; k > 0; k--)
            for (int i = 0; i < _width; i ++)        // создание карты местоположения
            {
                Vector3 alpha = new Vector3(i*8+_d*(k%2), k*_d+50, -3);   
                mapBalls.Add(alpha);
            }

        for (int i = 0; i < textMap.Length; i++)
        {
            switch (textMap.Substring(i, 1))
            {
                case "1":
                    CreateBall(redPool, i);
                    break;
                case "2":
                    CreateBall(bluePool, i);
                    break;
                case "3":
                    CreateBall(violetPool, i);
                    break;
                case "4":
                    CreateBall(turqoisePool, i);
                    break;
                case "9":
                    CreateBall(roofPool, i);
                    break;
            }
        }
    }
    private void CreateBall(Pool pool, int index)
    {
        GameObject unit = pool.Pop();
        if (pool == roofPool)
            roofSphers.Add(unit);
        unit.GetComponent<Ball>().awakePosition = unit.transform.position;
        unit.transform.position = mapBalls[index];
        unit.SetActive(true);
        counter++;
    }
    public void Apoptosis() // падение шариков, которые не связаны с "потолком" 
    {
        connected = new List<GameObject>();
        foreach (var alpha in roofSphers)
            if (!connected.Contains(alpha) && alpha!=null)
                alpha.GetComponent<Ball>().Apoptosis();              
        
        _balls = GameObject.FindGameObjectsWithTag("Balls");
        foreach (var ball in _balls)
            if (!connected.Contains(ball))
            {
                Destroy(ball.GetComponent<SpringJoint>());
                ball.GetComponent<SphereCollider>().enabled = false;
                ball.GetComponent<Ball>().StartFall();               
            }
        Remaining(counter, connected.Count);
    }
    private void OnDisable()
    {
        Ball.Reconfig -= Apoptosis;
        counter = 0;
    }

}
