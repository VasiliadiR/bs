﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repository : MonoBehaviour
{
    public static readonly int width = 14;
    public static readonly int height = 14;
    public static readonly float depth = -3;
    public static readonly float leftBorder = 0;
    public static readonly float rightBorder = 109;
    public static readonly float roof = 105;
    public static readonly float firstBottom = 25;
    public static readonly float bottom = -20;
    public static readonly int ballDistance = 4;
    public static readonly int minInfected = 3;
    public static List<GameObject> infected = new List<GameObject>();
    public static List<GameObject> witnesses = new List<GameObject>();


}
