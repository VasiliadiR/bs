﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System;

public class KickBallMotion : MonoBehaviour
{
    public static event Action Shoot;
    public static event Action Overflow;
    public GameObject staticPoint;
    public GameObject pixelForm;
    public Camera cam;

    private GameObject[] _balls;
    private RaycastHit _hitMouse;
    private RaycastHit _hitObject;

    private const float G = 0.00981f;   // гравитационаая составляющая
    private readonly float _tensionRange = 20;  //величина максимального натяжения
    private readonly float _spreading = 0.1f;   //коэффициент, отвечающий за скорость и разброс при максимальном натяжении
    private float _depth;
    private float _leftBorder;
    private float _rightBorder;
    private float _roof;
    private float _bottom;

    private List<Vector3> _pointsList = new List<Vector3>();
    private Vector3 _difference;
    private Vector3 _mainVector;
    private float _length;

    private bool _collisionTrig;
    private bool _touchTrig;    

    private void Awake()
    {
        Application.targetFrameRate = 60;
        _depth = Repository.depth;
        _leftBorder = Repository.leftBorder;
        _rightBorder = Repository.rightBorder;
        _roof = Repository.roof;
        _bottom = Repository.bottom;    
    }

    private void OnMouseDrag()
    {
        if (!_touchTrig && isActiveAndEnabled)    //невозможность управлять шариком после выстрела
        {
            _balls = GameObject.FindGameObjectsWithTag("Balls");
            Trajectory.box.SetActive(false);    
            Trajectory.box.SetActive(true);
            Raycast();
            _difference = staticPoint.transform.position - transform.position;
            _length = _difference.magnitude;  // зависимость скорости от натяжения
            _mainVector = _difference.normalized * _length * _spreading*2; //  сколько и куда пройдет шарик траектории за кадр. Разброс увеличен вдвое, чтобы один шарик траектории приходился на два вектора, которые проходит атакующий шарик
            Vector3 startPosition = transform.position;
            Vector3 startScale = pixelForm.transform.localScale;
            Vector3 gravity = Vector3.zero;
            int k = 0;
            for (int i = 0; i < 100; i++)
            {

                Trajectory.trajectoryList[i].transform.position = startPosition + (_mainVector * (i - k) + gravity * (i));
                if (Trajectory.trajectoryList[i].transform.position.x < _leftBorder || Trajectory.trajectoryList[i].transform.position.x > _rightBorder)
                {
                    _mainVector = Vector3.Reflect(_mainVector, Vector3.right);
                    startPosition = Trajectory.trajectoryList[i].transform.position - gravity * i;
                    k = i;
                }
                gravity.y -= G *2;

                Quaternion rotation = Quaternion.LookRotation(_mainVector); // ориентация шариков на вектор полета
                Trajectory.trajectoryList[i].transform.rotation = rotation;

                if (_length > _tensionRange - 1)    // уширение линии пути при максимальном натяжении
                    Trajectory.trajectoryList[i].transform.localScale = startScale + new Vector3(0, 6*_spreading * i, 6*_spreading * i);
                else
                    Trajectory.trajectoryList[i].transform.localScale = startScale;



                if (Trajectory.trajectoryList[i].transform.position.y < 0 || Trajectory.trajectoryList[i].transform.position.y > _roof ||
                    Array.Exists(_balls, point => (point.transform.position - Trajectory.trajectoryList[i].transform.position).magnitude
                    < (transform.localScale.y + Trajectory.trajectoryList[i].transform.localScale.y) / 2))  // линия траектории упирается в границы, либо в шары
                    break;
            }
            Vector3 addVector = _hitMouse.point;            //добавочный вектор, чтобы был на одной высоте
            addVector.Set(_hitMouse.point.x, _hitMouse.point.y, _depth);

            gameObject.transform.position = Vector3.Lerp(staticPoint.transform.position, addVector, // шарик следует за курсором, но не отдаляется дальше положенного
                (_tensionRange / (1 + (staticPoint.transform.position - addVector).magnitude)));

        }
    }

    void OnCollisionEnter(Collision collision)  // столкновение с другими шариками
    {
        if (!_collisionTrig && isActiveAndEnabled)
        {
            _collisionTrig = true;  // для обработки только первого столкновения
            StopAllCoroutines();
            ContactPoint contact = collision.GetContact(0); // первое столкновение                                              

            var ball = contact.otherCollider.GetComponent<Ball>();  //компонент, в котором есть ближайшие точки
            if (_length > _tensionRange - 1 && contact.otherCollider.TryGetComponent(out MeshFilter filter))    // пробиваем и заменяем шарик при полном натяжении
            {
                transform.position = ball.startPosition;
                contact.otherCollider.gameObject.SetActive(false);                 
            }
            else
            {
                Vector3 closest = ball.nearestPositions.OrderBy(x => (transform.position - x).magnitude).FirstOrDefault(); //поиск ближайшей точки
                gameObject.transform.position = closest;
            }
            gameObject.AddComponent<SpringJoint>(); // добавление атакующему шарику свойств статичных шариков
            var springJoint = gameObject.GetComponent<SpringJoint>();
            springJoint.spring = 1000;
            springJoint.damper = 100;
            springJoint.tolerance = 0;
            springJoint.anchor = transform.position;
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            Repository.infected = new List<GameObject>();
            gameObject.GetComponent<Ball>().Infection(gameObject, true);    //инициализация проверки соседних шариков
            gameObject.tag = "Balls";   // часть корабля - часть команды
        }       
    }
    void OnDisable()
    {
        _collisionTrig = false;
        _touchTrig = false;
    }
    private void OnMouseUp()
    {
        if (!_touchTrig && isActiveAndEnabled)    //невозможность управлять шариком после выстрела
        {
            Shoot();
            Trajectory.box.SetActive(false);    //выключить все шарики траектории
            _mainVector = _difference.normalized * _length * _spreading; //  сколько и куда пройдет шарик за кадр
            StartCoroutine(Movement(_mainVector));
        }
    }
    IEnumerator Movement(Vector3 mainVector)
    {
        _touchTrig = true;
        if (_length > _tensionRange - 1)
        {
            float rnd = UnityEngine.Random.Range(-2f, 2f);  // величина случайного отклонения
            mainVector = mainVector + new Vector3(rnd *_spreading * _difference.normalized.y, -rnd *_spreading * _difference.normalized.x, 0);  //вектор со случайным смещением
        }

        while (transform.position.y < _roof && transform.position.y > _bottom)
        {
            if (transform.position.x < _leftBorder || transform.position.x > _rightBorder)  // отражение от стен
                mainVector = Vector3.Reflect(mainVector, Vector3.right);

            transform.Translate(mainVector);
            mainVector.y -= G;
            yield return null;
        }
        if (transform.position.y > _bottom - 5)
        {
            gameObject.SetActive(false);
            if (gameObject.GetComponent<Ball>().lastTrig)
                Overflow();
        }
    }
    void Raycast() // получение координаты точки мыши для следования запускаемого шарика за курсором
    {
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        int layerMask = 1 << 1;
        layerMask = ~layerMask;
        Physics.Raycast(ray, out _hitMouse, Mathf.Infinity, layerMask);
    }
}
