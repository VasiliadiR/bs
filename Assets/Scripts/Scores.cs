﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scores : MonoBehaviour
{
    public static readonly int price = 10;
    public static readonly int countBalls = 20;
    public static readonly int percent = 30;
    public static int scores;
    public static float initialAmount;
    public static float currentAmount;
    public static Text scoreText;

    public GameObject winPanel;
    public GameObject losePanel;
    public Text winnerName;

    private string recordTable;
    private int _width;
    private void Start()
    {
        recordTable = "";
        _width = Repository.width;
        Map.Remaining += Percent;
        scoreText = GameObject.Find("Score").GetComponent<Text>();
    }
    private void Percent(int init, int cur)
    {
        initialAmount = init - _width;
        currentAmount = cur - _width;
        if (currentAmount / initialAmount * 100 < percent)  // в случае победы открыть соответствующую панель
            winPanel.SetActive(true);
        else if (CreateKickBall.number == -2)   // тоже для поражения
            losePanel.SetActive(true);
    }
    public void Table() //запись в таблицу рекордов
    {
        recordTable = PlayerPrefs.GetString("TableRecords").Length == 0 ? "" : PlayerPrefs.GetString("TableRecords"); 
        recordTable = recordTable.Insert(recordTable.Length, winnerName.text + " " + scores.ToString() + "\n");
        PlayerPrefs.SetString("TableRecords", recordTable);      
    }
    public static void ScoreUp(int k = 3)    // отображение очков на экране
    {
        scores += price;
        scores += (k - Repository.minInfected) * price / 5;   //награда за комбо
        scoreText.text = scores.ToString();
    }
    private void OnDisable()
    {
        Map.Remaining -= Percent;
        scores=0;
        initialAmount=0;
        currentAmount=0;
    }
}
