﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour
{
    public static GameObject[] trajectoryList;
    public static GameObject box;
    public GameObject pixelForm;

    private void Awake()
    {
        box = gameObject;
        trajectoryList = new GameObject[100];
        for (int i = 0; i < 100; i++)
        {
            GameObject pixel = Instantiate(pixelForm, transform);
            trajectoryList[i] = pixel;
        }
    }
}
