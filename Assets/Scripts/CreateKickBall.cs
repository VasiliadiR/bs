﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateKickBall : MonoBehaviour
{
    public Pool redPool;
    public Pool bluePool;
    public Pool violetPool;
    public Pool turqoisePool;
    public Text counter;
    public static int number = Scores.countBalls;
    public GameObject loserPanel;

    private GameObject[] _units = new GameObject[2];
    private Vector3 _secondBall = new Vector3(10, 14, -3);
    private GameObject _kickBall;


    void Start()
    {
        KickBallMotion.Shoot += RandomCreate;
        Ball.Overflow += GameOver;
        KickBallMotion.Overflow += GameOver;
        RandomCreate();
        RandomCreate();
    }
    void RandomCreate()
    {
        switch (Random.Range(1, 5))
        {
            case 1:
                CreateBall(redPool);
                break;
            case 2:
                CreateBall(bluePool);
                break;
            case 3:
                CreateBall(violetPool);
                break;
            case 4:
                CreateBall(turqoisePool);
                break;
        }
    }
    private void CreateBall(Pool pool)
    {
        if (number > 0)
        {
            _kickBall = pool.Pop();

            _kickBall.GetComponent<Ball>().awakePosition = _kickBall.transform.position;
            _kickBall.GetComponent<SphereCollider>().enabled = true;
            _kickBall.GetComponent<Rigidbody>().isKinematic = true;

            Destroy(_kickBall.GetComponent<SpringJoint>());
            _kickBall.tag = "Player";
            _kickBall.transform.position = _secondBall;
            _kickBall.SetActive(true);
        }
        if (number < Scores.countBalls && number > -1)
        {
            if (counter != null)
                counter.text = number.ToString();
            _units[1] = _units[0];
            _units[1].transform.position = gameObject.transform.position + new Vector3(0, -6, 0);
            _units[1].GetComponent<KickBallMotion>().enabled = true;
            if (number == 0)
                _units[1].GetComponent<Ball>().lastTrig = true;
        }
        _units[0] = _kickBall;
        number--;
    }
    private void GameOver()
    {
        loserPanel.SetActive(true);
    }
    private void OnDisable()
    {
        KickBallMotion.Shoot -= RandomCreate;
        Ball.Overflow -= GameOver;
        KickBallMotion.Overflow -= GameOver;
        number = Scores.countBalls;
    }
}
