﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Records : MonoBehaviour
{
    public Text recordTable;

    public void Start()
    {
        recordTable.text = PlayerPrefs.GetString("TableRecords");
    }
}
