﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fall : MonoBehaviour
{
    private const float G = 0.00981f;
    Vector3 down = new Vector3(0,G,0);
    void Update()
    {
        down.y -= G;
        gameObject.transform.position += down;
    }
}
