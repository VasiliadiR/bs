﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity;
using System;

public class Pool : MonoBehaviour
{
    public GameObject particle;
    public int size = 0;
    public List<GameObject> list = new List<GameObject>();
    private void Awake()
    {
        CreatePool();   // пул изначально состоит из неактивных объектов
    }
    public void CreatePool()
    {
        for (int k = 0; k < size; k++)
        {
            var unit = Instantiate(particle, gameObject.transform);
            unit.name = unit.name.Replace("(Clone)","") + list.Count;
            list.Add(unit);
        }
    }

    public GameObject Pop() // извлечение из пула идет по принципу первого свободного неактивного объекта 
    {
        for(int i=0; i<list.Count; i++)
            if (!list[i].activeSelf && this!=null)
                return list[i];
        return null;
    }
}