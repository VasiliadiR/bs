﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    public static event Action Reconfig;
    public static event Action Overflow;
    private const float G = 0.00981f;

    public BallType ballType;
    public ParticleSystem lightningBall;
    public Vector3 startPosition;
    public Vector3 awakePosition;
    public Vector3[] nearestPositions;
    public Vector3[] neighbors;
    public List<GameObject> sameBalls;
    public List<GameObject> closestBalls;
    public bool lastTrig = false;

    public GameObject[] balls;
    private float _depth;
    private float _leftBorder;
    private float _rightBorder;
    private float _roof;
    private float _bottom;
    private float _firstBottom;
    private int _d;
    public bool inTrig;
    
    private void Awake()
    {
        _depth = Repository.depth;
        _leftBorder = Repository.leftBorder;
        _rightBorder = Repository.rightBorder;
        _roof = Repository.roof;
        _bottom = Repository.bottom;
        _firstBottom = Repository.firstBottom;
        _d = Repository.ballDistance;
    }
    private void Start()
    {
        Reconfig += DefinePositions;
        DefinePositions();
    }
    bool FreeCell( Func<Vector3,bool> inBorder, Func<Vector3,bool> neighbor, Vector3 vec) => inBorder(vec) && !neighbor(vec);//свободные клетки
    bool InBorder(Vector3 vec) => vec.x < _rightBorder + 1 && vec.x > _leftBorder - 1 && vec.y < _roof + 1; //граничные условия
    bool Neighbor(Vector3 vec) => Array.Exists(balls, unit => (unit.transform.position - vec).magnitude < 2); // есть ли рядом соседи


    public void DefinePositions()   // добавление соседних позиций, которые может занимать выпущенный шарик
    {
         balls = GameObject.FindGameObjectsWithTag("Balls");
         sameBalls = new List<GameObject>();
         closestBalls = new List<GameObject>();
         nearestPositions = new Vector3[4];
         neighbors = new Vector3[4];
         startPosition = transform.position;

         AdVector(0, new Vector3(-_d, _d, 0));  //вектора, "окружающие" шарик
         AdVector(1, new Vector3(_d, -_d, 0));
         AdVector(2, new Vector3(-_d, -_d, 0));
         AdVector(3, new Vector3(_d, _d, 0));

        if (transform.position.y < _firstBottom && closestBalls.Count!=0)
            Overflow();
    }
    public void Infection(GameObject transmitter = null, bool zeroTrig = false) // проверка соседних шариков на то, одного ли они цвета рекурсивным методом
    {
        if (!Repository.infected.Contains(gameObject))
        {
            DefinePositions();
            Repository.infected.Add(gameObject);
            foreach (var ball in sameBalls)
            {
                if (ball != null && ball != transmitter)
                    ball.GetComponent<Ball>().Infection(gameObject);
            }
            if (zeroTrig && Repository.infected.Count > Repository.minInfected - 1)  // число шариков одного цвета
                for (int i = 0; i < Repository.infected.Count; i++)
                    Repository.infected[i].GetComponent<Ball>().Fireworks(i);
            else if (lastTrig)
                Overflow();
        }
    }
    public void Apoptosis() // проверка шариков на закрепленность к "потолку" рекурсивным методом
    {
        if (!Map.connected.Contains(gameObject))
        {
            DefinePositions();
            Map.connected.Add(gameObject);
            foreach (var sphere in closestBalls)
                sphere.GetComponent<Ball>().Apoptosis();
        }
    }
    private void AdVector(int i, Vector3 plusVector)
    {
       if(FreeCell(InBorder, Neighbor, startPosition + plusVector)) //свободные клетки
            nearestPositions[i] = startPosition + plusVector;
        else
            nearestPositions[i] = Vector3.zero; // если шарик находится на границе и одна из его позиций "выпадает" за границу, то она становится нулевым вектором
                                                //для выпущенного шарика, ищущего ближайшую позицию - нулевой вектор будет самым дальним
        if (Neighbor(startPosition + plusVector))   // клетки, в которых соседние шарики
        {
            neighbors[i] = startPosition + plusVector;  
            closestBalls.Add(Array.Find(balls, unit => (unit.transform.position - (startPosition + plusVector)).magnitude < 2)); //соседние шарики

            sameBalls.Add(Array.Find(balls, unit => (unit.transform.position - (startPosition + plusVector)).magnitude < 2 && //соседние шарики того же цвета
                (unit.GetComponent<Ball>().ballType == gameObject.GetComponent<Ball>().ballType)));
        }
        else
            neighbors[i] = Vector3.zero;
    }
    private void OnDisable()
    {
        transform.position = awakePosition;
        Reconfig -= DefinePositions;
    }
    public void Fireworks(int index)    //анимация уничтожения
    {
        lightningBall.Play();
        StartCoroutine(Deactivate(lightningBall.main.duration + 0.5f, index));
    }
    public IEnumerator Deactivate(float time, int index) // уничтожение 
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
        Scores.ScoreUp(Repository.infected.Count);
        if (index == Repository.infected.Count - 1)
            Reconfig();
    }
    public void StartFall() //падение незакрепленных шариков
    {
        StartCoroutine(Falling());
    }
    public IEnumerator Falling()
    {
        Vector3 down = Vector3.zero;
        while(transform.position.y > _bottom)
        {
            down.y -= G;
            transform.position += down;
            yield return null;
        }
        Scores.ScoreUp();
        gameObject.SetActive(false);
    }
}

