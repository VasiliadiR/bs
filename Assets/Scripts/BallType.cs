﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BallType 
{
    Blue,
    Red,
    Turquoise,
    Violet,
    Invisible
}
